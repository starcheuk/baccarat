﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dealer : MonoBehaviour {

// Simulating shuffle cards and deal cards
	public Sprite[] SpadesImage;
	public Sprite[] HeartsImage;
	public Sprite[] DiamondsImage;
	public Sprite[] ClubsImage;

	List<Card> Cards = new List<Card>();

	public void GenerateNewCards(){

        Cards.Clear();
		
		for (int j = 0; j < 8; j++)
		{
			for (int i = 0; i < SpadesImage.Length; i++)
            {
                int cardValue = (i < 9) ? i + 1 : 0;
                Card newCard = new Card { CardNum = i + 1, CardValue = cardValue, CardImage = SpadesImage[i] };
                Cards.Add(newCard);
            }

            for (int i = 0; i < HeartsImage.Length; i++)
            {
                int cardValue = (i < 9) ? i + 1 : 0;
                Card newCard = new Card { CardNum = i + 1, CardValue = cardValue, CardImage = HeartsImage[i] };
                Cards.Add(newCard);
            }

            for (int i = 0; i < DiamondsImage.Length; i++)
            {
                int cardValue = (i < 9) ? i + 1 : 0;
                Card newCard = new Card { CardNum = i + 1, CardValue = cardValue, CardImage = DiamondsImage[i] };
                Cards.Add(newCard);
            }

            for (int i = 0; i < ClubsImage.Length; i++)
            {
                int cardValue = (i < 9) ? i + 1 : 0;
                Card newCard = new Card { CardNum = i + 1, CardValue = cardValue, CardImage = ClubsImage[i] };
                Cards.Add(newCard);
            }
		}
	}

	public Card RandCard(){
		int randomNum = Random.Range(0, Cards.Count);
		Card randomCard = Cards[randomNum];
		Cards.RemoveAt(randomNum);

		return randomCard;
	}
}
