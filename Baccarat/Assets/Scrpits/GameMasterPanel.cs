﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMasterPanel : MonoBehaviour {

	public Button StartMiBtn;
	public GameObject BettingPanel;

	Animator anim;
	bool GameMasterMode;

	void Start(){
		anim = GetComponent<Animator>();
	}

	public void GameMasterBtnPressed(){
		if(!GameMasterMode){
			GameMasterMode = true;
			StartMiBtn.interactable = false;
			BettingPanel.SetActive(false);
			anim.SetTrigger("TurnGameMaster");
		}else{
			GameMasterMode = false;
			anim.SetTrigger("TurnGamePlay");
		}
	}

	public void ShowCardResultPanel(){
		StartMiBtn.interactable = true;
		BettingPanel.SetActive(true);
	}

}
