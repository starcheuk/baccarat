﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum MiCardType{BankerMiCard, PlayerMiCard};

public class GamePlayManager : MonoBehaviour {

    public RoadsManager roadsManager;
	public Dealer dealer;
    public GameObject MiCardPanel;
    public GameObject ResultPanel;

    #region MiCardStruct
        [System.Serializable]
        public struct MiCardStuff{
            public MiCard miCardBody;
            public bool MiCardHasValue;
            public int MiCardValue;
            public int MiCardNum;
        }
        [System.Serializable]
        public struct MiCardGroup{
            public MiCardStuff[] miCardStuff;
            public Button OpenBothCardBtn;
            public Button SetMiCardBtn;
            public Toggle PairToggle;
            public GameObject CardsPH;
            public GameObject BtnsPH;
            public GameObject Card3PH;
            public GameObject Card3BtnPH;
            public GameObject Card3OfMiCardResult;
            public bool NoNeedAddCard;
        }
    #endregion

	public MiCardGroup PlayerMiCard;
	public MiCardGroup BankerMiCard;

    private bool FirstRoundMatched;

	public void DealCards(){
		for (int i = 0; i < 2; i++)
		{
			Card newCard;
			newCard = dealer.RandCard();
			PlayerMiCard.miCardStuff[i].miCardBody.SetMiCard(newCard.CardNum, newCard.CardValue, newCard.CardImage);
			newCard = dealer.RandCard();
			BankerMiCard.miCardStuff[i].miCardBody.SetMiCard(newCard.CardNum, newCard.CardValue, newCard.CardImage);
		}
	}

	public void MiCardOpened(MiCardType miCardType, int cardSerial, int cardValue, int cardNum){
		
		switch(miCardType){
            case MiCardType.PlayerMiCard:
				PlayerMiCard.miCardStuff[cardSerial].MiCardHasValue = true;
				PlayerMiCard.miCardStuff[cardSerial].MiCardValue = cardValue;
                PlayerMiCard.miCardStuff[cardSerial].MiCardNum = cardNum;
				CheckState();
                break;
            case MiCardType.BankerMiCard:
				BankerMiCard.miCardStuff[cardSerial].MiCardHasValue = true;
				BankerMiCard.miCardStuff[cardSerial].MiCardValue = cardValue;
                BankerMiCard.miCardStuff[cardSerial].MiCardNum = cardNum;
				CheckState();
                break;
		}
	}

	private void CheckState(){
		if(PlayerMiCard.miCardStuff[0].MiCardHasValue && PlayerMiCard.miCardStuff[1].MiCardHasValue) PlayerMiCard.OpenBothCardBtn.interactable = false;
		if(BankerMiCard.miCardStuff[0].MiCardHasValue && BankerMiCard.miCardStuff[1].MiCardHasValue) BankerMiCard.OpenBothCardBtn.interactable = false;

		if(PlayerMiCard.miCardStuff[0].MiCardHasValue &&
			PlayerMiCard.miCardStuff[1].MiCardHasValue &&
			BankerMiCard.miCardStuff[0].MiCardHasValue &&
			BankerMiCard.miCardStuff[1].MiCardHasValue){

            int playerValue = 0;
            int bankerValue = 0;

            for (int i = 0; i < 3; i++)
            {
                playerValue += PlayerMiCard.miCardStuff[i].MiCardValue;
                bankerValue += BankerMiCard.miCardStuff[i].MiCardValue;
            }

            playerValue = playerValue % 10;
            bankerValue = bankerValue % 10;

			if (!FirstRoundMatched)
			{

                if ((playerValue >= 8 || bankerValue >= 8))
                {
					PlayerMiCard.NoNeedAddCard = true;
					BankerMiCard.NoNeedAddCard = true;
                }else{
					if(playerValue <= 5){
						PlayerAddCard();
					}else{
						PlayerMiCard.NoNeedAddCard = true;
					}

					if(bankerValue <= 2){
						BankerAddCard();
					}
				}
                FirstRoundMatched = true;
			}

            if (!BankerMiCard.NoNeedAddCard && !BankerMiCard.Card3PH.activeSelf)
            {
                bool playerCard3HasValue = PlayerMiCard.miCardStuff[2].MiCardHasValue;
                bool bankerCard3HasValue = BankerMiCard.miCardStuff[2].MiCardHasValue;
                int playerCard3Value = PlayerMiCard.miCardStuff[2].MiCardValue;

                switch (bankerValue)
                {
                    case 3:
                        if (!bankerCard3HasValue && playerCard3HasValue && playerCard3Value == 8)
                        {
                            BankerMiCard.NoNeedAddCard = true;
                        }
                        else if (!bankerCard3HasValue)
                        {
                            BankerAddCard();
                        }
                        else
                        {
                            BankerMiCard.NoNeedAddCard = true;
                        }
                        break;
                    case 4:
                        if (!bankerCard3HasValue && playerCard3HasValue && (playerCard3Value == 0 || playerCard3Value == 1 || playerCard3Value == 8 || playerCard3Value == 9))
                        {
                            BankerMiCard.NoNeedAddCard = true;
                        }
                        else if (!bankerCard3HasValue)
                        {
                            BankerAddCard();
                        }
                        else
                        {
                            BankerMiCard.NoNeedAddCard = true;
                        }
                        break;
                    case 5:
                        if (!bankerCard3HasValue && playerCard3HasValue && (playerCard3Value == 0 || playerCard3Value == 1 || playerCard3Value == 2 || playerCard3Value == 3 || playerCard3Value == 8 || playerCard3Value == 9))
                        {
                            BankerMiCard.NoNeedAddCard = true;
                        }
                        else if (!bankerCard3HasValue)
                        {
                            BankerAddCard();
                        }
                        else
                        {
                            BankerMiCard.NoNeedAddCard = true;
                        }
                        break;
                    case 6:
                        if (!bankerCard3HasValue && playerCard3HasValue && (playerCard3Value == 6 || playerCard3Value == 7))
                        {
                            BankerAddCard();
                        }
                        else
                        {
                            BankerMiCard.NoNeedAddCard = true;
                        }
                        break;
                    case 7:
                        BankerMiCard.NoNeedAddCard = true;
                        break;

                }
			}
            

			if(PlayerMiCard.miCardStuff[2].MiCardHasValue) PlayerMiCard.NoNeedAddCard = true;
			if(BankerMiCard.miCardStuff[2].MiCardHasValue) BankerMiCard.NoNeedAddCard = true;
            
			if(PlayerMiCard.NoNeedAddCard && BankerMiCard.NoNeedAddCard) CheckWinner(playerValue, bankerValue);
		}
	}

    private void PlayerAddCard()
    {
        PlayerMiCard.CardsPH.SetActive(false);
        PlayerMiCard.BtnsPH.SetActive(false);
        PlayerMiCard.Card3PH.SetActive(true);
        PlayerMiCard.Card3BtnPH.SetActive(true);
        Card newCard = dealer.RandCard();
        PlayerMiCard.miCardStuff[2].miCardBody.SetMiCard(newCard.CardNum, newCard.CardValue, newCard.CardImage);
        PlayerMiCard.Card3OfMiCardResult.SetActive(true);
    }

    private void BankerAddCard()
    {
        BankerMiCard.CardsPH.SetActive(false);
        BankerMiCard.BtnsPH.SetActive(false);
        BankerMiCard.Card3PH.SetActive(true);
        BankerMiCard.Card3BtnPH.SetActive(true);
        Card newCard = dealer.RandCard();
        BankerMiCard.miCardStuff[2].miCardBody.SetMiCard(newCard.CardNum, newCard.CardValue, newCard.CardImage);
        BankerMiCard.Card3OfMiCardResult.SetActive(true);
    }

	private void CheckWinner(int playerValue, int bankerValue){

        if(PlayerMiCard.miCardStuff[0].MiCardNum == PlayerMiCard.miCardStuff[1].MiCardNum){
            PlayerMiCard.PairToggle.isOn = true;
        }else{
            PlayerMiCard.PairToggle.isOn = false;
        }
        if(BankerMiCard.miCardStuff[0].MiCardNum == BankerMiCard.miCardStuff[1].MiCardNum){
            BankerMiCard.PairToggle.isOn = true;
        }else{
            BankerMiCard.PairToggle.isOn = false;
        }
        
		if(playerValue == bankerValue){
            roadsManager.TieBtnPressed();
            Invoke("ResetMiCard", 5f);
		}else{
			if(playerValue > bankerValue){
                roadsManager.PlayerBtnPressed();
                Invoke("ResetMiCard", 5f);
			}else{
                roadsManager.BankerBtnPressed();
                Invoke("ResetMiCard", 5f);
			}
		}
		
	}

    private void ResetMiCard(){
        PlayerMiCard.SetMiCardBtn.interactable = true;
        BankerMiCard.SetMiCardBtn.interactable = true;
        PlayerMiCard.OpenBothCardBtn.interactable = true;
        BankerMiCard.OpenBothCardBtn.interactable = true;

        for (int i = 0; i < 3; i++)
        {
            PlayerMiCard.miCardStuff[i].miCardBody.ResetCard();
            BankerMiCard.miCardStuff[i].miCardBody.ResetCard();
            PlayerMiCard.miCardStuff[i].MiCardHasValue = false;
            BankerMiCard.miCardStuff[i].MiCardHasValue = false;
            PlayerMiCard.miCardStuff[i].MiCardValue = 0;
            BankerMiCard.miCardStuff[i].MiCardValue = 0;
        }

        PlayerMiCard.CardsPH.SetActive(true);
        BankerMiCard.CardsPH.SetActive(true);
        PlayerMiCard.BtnsPH.SetActive(true);
        BankerMiCard.BtnsPH.SetActive(true);

        PlayerMiCard.Card3PH.SetActive(false);
        BankerMiCard.Card3PH.SetActive(false);
        PlayerMiCard.Card3BtnPH.SetActive(false);
        BankerMiCard.Card3BtnPH.SetActive(false);

        PlayerMiCard.Card3OfMiCardResult.SetActive(false);
        BankerMiCard.Card3OfMiCardResult.SetActive(false);
        PlayerMiCard.NoNeedAddCard = false;
        BankerMiCard.NoNeedAddCard = false;

        MiCardPanel.SetActive(false);
        ResultPanel.SetActive(false);

        FirstRoundMatched = false;
    }
	
}
