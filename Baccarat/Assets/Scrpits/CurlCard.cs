﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PointerTriggerLocation{ Blank, BottomLeft, BottomRight }

public class CurlCard : MonoBehaviour {

	public Vector3 AppearPos;
	public CurlCard[] OtherTrigger;

	public Image CardBG;
	public Transform CurlCardMask;
	public Transform BackCard;
	public Transform FrontCard;
	public Transform Shadow;

	private float offsetX;
    private float offsetY;
	private bool isDragging;
	private Vector2 StartDraggingPoint;

	private void UpdateCurlCard(){
		Vector3 BackCardPos = BackCard.position;
		Vector3 BackCardRotation = BackCard.gameObject.transform.eulerAngles;
		float CardBGRoatation = CardBG.gameObject.transform.eulerAngles.z;

        Vector3 pos = FrontCard.localPosition;
        float theta = Mathf.Atan2(pos.y, pos.x) * 180.0f / Mathf.PI;

        // if(theta <= 0.0f || theta >= 90.0f) return;

        float deg = -(90.0f - theta) * 2.0f;
        FrontCard.eulerAngles = new Vector3(0.0f, 0.0f, deg + CardBGRoatation);

        CurlCardMask.position = (BackCard.position + FrontCard.position) * 0.5f;
        CurlCardMask.eulerAngles = new Vector3(0.0f, 0.0f, deg * 0.5f + CardBGRoatation);

        Shadow.position = CurlCardMask.position;
        Shadow.eulerAngles = new Vector3(0.0f, 0.0f, deg * 0.5f + 90.0f + CardBGRoatation);

        BackCard.position = BackCardPos;
        BackCard.gameObject.transform.eulerAngles = BackCardRotation;
	}

	public void OnPointerEnter(){
		if(!isDragging){
            CardBG.enabled = false;
            CurlCardMask.gameObject.SetActive(true);
            FrontCard.localPosition = AppearPos;
			UpdateCurlCard();
			Shadow.gameObject.SetActive(false);
		}
		
	}

	public void OnPointerExit(){
		if(!isDragging){
            FrontCard.localPosition = new Vector3(0, 0, 0);
            CurlCardMask.gameObject.SetActive(false);
            CardBG.enabled = true;
			UpdateCurlCard();
			Shadow.gameObject.SetActive(false);
		} 
	}

	public void OnBeginDrag(){
		isDragging = true;
		for (int i = 0; i < OtherTrigger.Length; i++)
		{
			OtherTrigger[i].SetIsDragging();
		}
		StartDraggingPoint = Input.mousePosition;
		offsetX = FrontCard.transform.position.x - Input.mousePosition.x;
        offsetY = FrontCard.transform.position.y - Input.mousePosition.y;
	}

	public void OnDrag(){
        if (isDragging)
        {
            if (Vector2.Distance(StartDraggingPoint, Input.mousePosition) > CardBG.gameObject.GetComponent<RectTransform>().rect.size.x * 0.8f)
            {
                OnEndDrag();
                CardBG.gameObject.GetComponent<MiCard>().OpenCard();
            }
            else
            {
                FrontCard.position = new Vector3(offsetX + Input.mousePosition.x, offsetY + Input.mousePosition.y);
                UpdateCurlCard();
                Shadow.gameObject.SetActive(true);
            }
        }
		
	}

	public void OnEndDrag(){
		isDragging = false;
		for (int i = 0; i < OtherTrigger.Length; i++)
		{
			OtherTrigger[i].SetIsDragging();
		}
		FrontCard.localPosition = new Vector3(0, 0, 0);
		CurlCardMask.gameObject.SetActive(false);
        CardBG.enabled = true;
	}

	public void SetIsDragging(){
		isDragging = (isDragging) ? false : true;
	}

	//set pivot without changing position
    public static void SetPivot(RectTransform rectTransform, Vector2 pivot)
    {
        if (rectTransform == null)
            return;

        Vector3 scale = rectTransform.localScale;
        Vector2 size = rectTransform.rect.size;
        Vector2 deltaPivot = rectTransform.pivot - pivot;
        Vector3 deltaPosition = new Vector3(deltaPivot.x * size.x * scale.x, deltaPivot.y * size.y * scale.y);
        rectTransform.pivot = pivot;
        rectTransform.localPosition -= deltaPosition;
    }
}
