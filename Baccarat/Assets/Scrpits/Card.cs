﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public struct Card{
	
	public int CardNum; 
	public int CardValue;
	public Sprite CardImage;

}
