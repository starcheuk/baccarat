﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiCard : MonoBehaviour {

	public MiCardType miCardType;
	public GamePlayManager gamePlayManager;
	public int CardSerial;
	public Image[] CurlImage;
	public GameObject CurlTriggers;
	public Button OpenBtn;
	public Image ResultCard;
	public Text ResultText;
	public Sprite BlankSprite;
	

	private int CardNum;
	private int CardValue;
	private bool HasOpened;
	private Image SelfImage;

	private RectTransform rectTransform;
	private Sprite StandbySprite;
	
	void Start(){
		rectTransform = GetComponent<RectTransform>();
		SelfImage = GetComponent<Image>();
	}

	public void RotateCard(){
		rectTransform.Rotate(new Vector3(0,0,90));
	}

	public void SetMiCard(int cardNum, int cardValue, Sprite sprite){
		CardNum = cardNum;
		CardValue = cardValue;
		StandbySprite = sprite;
		for (int i = 0; i < CurlImage.Length; i++)
		{
			CurlImage[i].sprite = sprite;
		}
	}

	public int GetCardValue(){
		return CardValue;
	}

	public void OpenCard(){
		if(!HasOpened){
			HasOpened = true;
			SelfImage.enabled = true;
			SelfImage.sprite = StandbySprite;
            ResultCard.sprite = StandbySprite;
            int newValue = (((ResultText.text == "") ? 0 : int.Parse(ResultText.text)) + CardValue) % 10;
            ResultText.text = newValue.ToString();
            OpenBtn.interactable = false;

			gamePlayManager.MiCardOpened(miCardType, CardSerial, CardValue, CardNum);
			CurlTriggers.SetActive(false);
		}
	}

	public void ResetCard(){
		CurlTriggers.SetActive(true);
		transform.eulerAngles = Vector3.zero;
		OpenBtn.interactable = true;
		GetComponent<Image>().sprite = BlankSprite;
		ResultCard.sprite = BlankSprite;
		ResultText.text = "";
		HasOpened = false;
	}

	

}
