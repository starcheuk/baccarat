﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoadBoardBuilder : MonoBehaviour {

    // Inspector setting
    public GameObject tile; // Drag a Tile prefab of the prototype of road boards
    public int xSize, ySize; // Set the size of road boards
    public RectTransform RoadNameText; // Drag the text of road name to calculate the screen size of road board
    public RoadBoardType roadBoardType; // Set the layout type of road board

    // Gobal Variables
    public GameObject[,] RoadTiles; // To save every single tiles of current road board
    public float tileSize; 

    // Class Variables
    private RectTransform ParentRect; 
    private GridLayoutGroup gridLayoutGroup;

    void Start () {

        ParentRect = transform.parent.GetComponent<RectTransform>();
        gridLayoutGroup = GetComponent<GridLayoutGroup>();

        switch (roadBoardType)
        {
            case RoadBoardType.Vertical:
                tileSize = (ParentRect.rect.height- RoadNameText.sizeDelta.y - (gridLayoutGroup.spacing.x * ySize) - 10) / ySize;
                break;
            case RoadBoardType.Horizontal:
                tileSize = (ParentRect.rect.height - (gridLayoutGroup.spacing.x * (ySize - 1)) - 10) / ySize;
                break;
        }
        
        gridLayoutGroup.cellSize = new Vector2(tileSize, tileSize); 

        Rect tileRect = tile.GetComponent<RectTransform>().rect;
        tileRect.width = tileSize;
        tileRect.height = tileSize;

        CreateMainRoad();
    }

    private void CreateMainRoad()
    {
        RoadTiles = new GameObject[xSize, ySize];
        float parentWidth = 0;

        for (int x = 0; x < xSize; x++)
        {
            parentWidth = parentWidth + tileSize + gridLayoutGroup.spacing.x;
            for (int y = 0; y < ySize; y++)
            {
                GameObject newTile = Instantiate(tile, tile.transform.position, tile.transform.rotation) as GameObject;
                RoadTiles[x, y] = newTile;
                newTile.transform.SetParent(transform);
                newTile.GetComponent<Tile>().SetResult(Result.Blank, true);
                newTile.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            }
        }

        Vector2 newSize = new Vector2(0,0);

        switch (roadBoardType)
        {
            case RoadBoardType.Vertical:
                newSize = new Vector2(parentWidth + 10, ParentRect.sizeDelta.y);
                transform.position -= new Vector3(0, RoadNameText.sizeDelta.y / 2 - 5, 0);
                
                break;
            case RoadBoardType.Horizontal:
                newSize = new Vector2(RoadNameText.rect.width + parentWidth + 10, ParentRect.sizeDelta.y);
                break;
        }
        
        ParentRect.sizeDelta = newSize;

    }

    public enum RoadBoardType
    {
        Vertical, Horizontal
    }

    public GameObject[,] GetRoadTiles(){ return RoadTiles;}
}
