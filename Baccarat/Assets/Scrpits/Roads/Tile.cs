﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Tile : MonoBehaviour {

    public Image TileResultImage;
    public Text TileResultText;
    public GameObject[] Ties;
    public Color BlankerColor;
    public Color BankerPillColor;
    public Color PlayerPillColor;
    public Color TiePillColor;
    public GameObject BankerPair;
    public GameObject PlayerPair;

    private Result TileResult;

    Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void ResetPillAfterAnim()
    {
        TileResultText.text = "";
        TileResultImage.color = BlankerColor;
        TileResultImage.gameObject.SetActive(false);
        GameObject.Find("BankerBtn").GetComponent<Button>().interactable = true;
        GameObject.Find("PlayerBtn").GetComponent<Button>().interactable = true;
        GameObject.Find("TieBtn").GetComponent<Button>().interactable = true;
        GameObject.Find("BankerPredictorBtn").GetComponent<Button>().interactable = true;
        GameObject.Find("PlayerPredictorBtn").GetComponent<Button>().interactable = true;
    }

    public void SetResult(Result result, bool predictorMode)
    {
        if (!predictorMode)
        {
            TileResult = result;

            switch (result)
            {
                case Result.Blank:
                    TileResultImage.color = BlankerColor;
                    TileResultImage.gameObject.SetActive(false);
                    break;
                case Result.Banker:
                    TileResultImage.color = BankerPillColor;
                    TileResultImage.gameObject.SetActive(true);
                    break;
                case Result.Player:
                    TileResultImage.color = PlayerPillColor;
                    TileResultImage.gameObject.SetActive(true);
                    break;
            }
        }
        else
        {
            switch (result)
            {
                case Result.Banker:
                    TileResultImage.color = BankerPillColor;
                    TileResultImage.gameObject.SetActive(true);
                    anim.SetTrigger("SetPredict");
                    break;
                case Result.Player:
                    TileResultImage.color = PlayerPillColor;
                    TileResultImage.gameObject.SetActive(true);
                    anim.SetTrigger("SetPredict");
                    break;
            }
        }
    }

    public void SetResultManual(Result result, bool predictorMode)
    {
        if (!predictorMode)
        {
            TileResult = result;

            switch (result)
            {
                case Result.Blank:
                    TileResultImage.color = BlankerColor;
                    TileResultText.text = "";
                    BankerPair.SetActive(false);
                    PlayerPair.SetActive(false);
                    TileResultImage.gameObject.SetActive(false);
                    break;
                case Result.Banker:
                    TileResultImage.color = BankerPillColor;
                    TileResultText.text = "莊";
                    TileResultImage.gameObject.SetActive(true);
                    CheckPair();
                    break;
                case Result.Player:
                    TileResultImage.color = PlayerPillColor;
                    TileResultText.text = "閒";
                    TileResultImage.gameObject.SetActive(true);
                    CheckPair();
                    break;
                case Result.Tie:
                    TileResultImage.color = TiePillColor;
                    TileResultText.text = "和";
                    TileResultImage.gameObject.SetActive(true);
                    CheckPair();
                    break;

            }
        }
        else
        {
            switch (result)
            {
                case Result.Banker:
                    TileResultImage.color = BankerPillColor;
                    TileResultText.text = "莊";
                    TileResultImage.gameObject.SetActive(true);
                    anim.SetTrigger("SetPredict");
                    break;
                case Result.Player:
                    TileResultImage.color = PlayerPillColor;
                    TileResultText.text = "閒";
                    TileResultImage.gameObject.SetActive(true);
                    anim.SetTrigger("SetPredict");
                    break;
            }
        }
    }

    private void CheckPair(){
        BankerPair.SetActive(GameObject.Find("BankerPairToggle").GetComponent<Toggle>().isOn);
        PlayerPair.SetActive(GameObject.Find("PlayerPairToggle").GetComponent<Toggle>().isOn);
    }

    public void SetTie(int tieTimes){
        if(tieTimes <= 4)
        {
            switch (tieTimes)
            {
                case 0:
                    Ties[tieTimes].SetActive(true);
                    break;
                case 1:
                    Ties[tieTimes].SetActive(true);
                    break;
                case 2:
                    Ties[tieTimes].SetActive(true);
                    break;
                case 3:
                    Ties[tieTimes].SetActive(true);
                    break;
            }
        }
    }


    public Result GetResult(){ return TileResult;}

    public bool CompareResult(Result result){ return TileResult == result;}
}
