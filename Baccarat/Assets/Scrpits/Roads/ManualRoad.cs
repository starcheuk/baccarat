﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManualRoad : RoadBoard {

	public void CreatePill(Result result, bool predictorMode){

		Tile tile = roadBoardBuilder.RoadTiles[(int)CurrentPoint.x, (int)CurrentPoint.y].GetComponent<Tile>();
		tile.SetResultManual(result, predictorMode);

		if(!predictorMode){
			if(CurrentPoint.y >= 5){
				CurrentPoint = new Vector2(CurrentPoint.x + 1, 0);
			}else{
				CurrentPoint.y++;
			}
		}

	}

	public void BankerBtnPressed(){
		Result result = Result.Banker;
		bool predictorMode = false;
		CreatePill(result, predictorMode);
	}

	public void BankerPredictorBtnPressed(){
		Result result = Result.Banker;
		bool predictorMode = true;
		CreatePill(result, predictorMode);
	}

	public void PlayerBtnPressed(){
		Result result = Result.Player;
		bool predictorMode = false;
		CreatePill(result, predictorMode);
	}

	public void PlayerPredictorBtnPressed(){
		Result result = Result.Player;
		bool predictorMode = true;
		CreatePill(result, predictorMode);
	}

	public void TieBtnPressed(){
		Result result = Result.Tie;
		bool predictorMode = false;
		CreatePill(result, predictorMode);
	}

}
