﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainRoad : RoadBoard {

	private int TieTimes = 0;

	public void BankerBtnPressed(){
		TieTimes = 0;
        bool predictorMode = false;
        Result result = Result.Banker;
		Vector2 newPillPoint = GetNewPillPoint(result);
		CreatePill(result, newPillPoint, predictorMode);
	}

	public void BankerPredictorBtnPressed(){
        bool predictorMode = true;
        Result result = Result.Banker;
		Vector2 newPillPoint = GetNewPillPoint(result);
		CreatePill(result, newPillPoint, predictorMode);
	}

	public void PlayerBtnPressed(){
		TieTimes = 0;
        bool predictorMode = false;
        Result result = Result.Player;
		Vector2 newPillPoint = GetNewPillPoint(result);
		CreatePill(result, newPillPoint, predictorMode);
	}

	public void PlayerPredictorBtnPressed(){
        bool predictorMode = true;
        Result result = Result.Player;
		Vector2 newPillPoint = GetNewPillPoint(result);
		CreatePill(result, newPillPoint, predictorMode);
	}

	public void TieBtnPressed(){
		if(PreviousResult != Result.Blank){
			
			Tile tile = GetComponent<RoadBoardBuilder>().RoadTiles[(int)CurrentPoint.x, (int)CurrentPoint.y].GetComponent<Tile>();
			tile.SetTie(TieTimes);

			TieTimes++;
		}
	}
}
