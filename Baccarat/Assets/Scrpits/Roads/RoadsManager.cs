﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Result { Blank, Banker, Player, Tie }

public class RoadsManager : MonoBehaviour {

	public MainRoad mainRoad;

	public RoadBoard BigEyeRoad;
	public RoadBoard SmallRoad;
	public RoadBoard CockroachRoad;

	public ManualRoad manualRoad;


	public void BankerBtnPressed(){
		mainRoad.BankerBtnPressed();
		GameObject[,] mainRoadTiles = mainRoad.gameObject.GetComponent<RoadBoardBuilder>().GetRoadTiles();
		Vector2 mainRoadCurrentPoint = mainRoad.getCurrentPoint();

		BigEyeRoad.BankerBtnPressed(mainRoadTiles, mainRoadCurrentPoint);
		SmallRoad.BankerBtnPressed(mainRoadTiles, mainRoadCurrentPoint);
		CockroachRoad.BankerBtnPressed(mainRoadTiles,mainRoadCurrentPoint);

		manualRoad.BankerBtnPressed();
	}

	public void BankerPredictorBtnPressed(){
		mainRoad.BankerPredictorBtnPressed();
		GameObject[,] mainRoadTiles = mainRoad.gameObject.GetComponent<RoadBoardBuilder>().GetRoadTiles();
		Vector2 mainRoadCurrentPoint = mainRoad.getCurrentPoint();

		BigEyeRoad.BankerPredictorBtnPressed(mainRoadTiles, mainRoadCurrentPoint);
		SmallRoad.BankerPredictorBtnPressed(mainRoadTiles, mainRoadCurrentPoint);
		CockroachRoad.BankerPredictorBtnPressed(mainRoadTiles, mainRoadCurrentPoint);

		manualRoad.BankerPredictorBtnPressed();
	}

	public void PlayerBtnPressed(){
		mainRoad.PlayerBtnPressed();
		GameObject[,] mainRoadTiles = mainRoad.gameObject.GetComponent<RoadBoardBuilder>().GetRoadTiles();
		Vector2 mainRoadCurrentPoint = mainRoad.getCurrentPoint();

		BigEyeRoad.PlayerBtnPressed(mainRoadTiles, mainRoadCurrentPoint);
		SmallRoad.PlayerBtnPressed(mainRoadTiles, mainRoadCurrentPoint);
		CockroachRoad.PlayerBtnPressed(mainRoadTiles, mainRoadCurrentPoint);

		manualRoad.PlayerBtnPressed();
	}

	public void PlayerPredictorBtnPressed(){
		mainRoad.PlayerPredictorBtnPressed();
		GameObject[,] mainRoadTiles = mainRoad.gameObject.GetComponent<RoadBoardBuilder>().GetRoadTiles();
		Vector2 mainRoadCurrentPoint = mainRoad.getCurrentPoint();

		BigEyeRoad.PlayerPredictorBtnPressed(mainRoadTiles, mainRoadCurrentPoint);
		SmallRoad.PlayerPredictorBtnPressed(mainRoadTiles, mainRoadCurrentPoint);
		CockroachRoad.PlayerPredictorBtnPressed(mainRoadTiles, mainRoadCurrentPoint);

		manualRoad.PlayerPredictorBtnPressed();
	}

	public void TieBtnPressed(){
		mainRoad.TieBtnPressed();

		manualRoad.TieBtnPressed();
	}

}
