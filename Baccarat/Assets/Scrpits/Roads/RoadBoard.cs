﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadBoard : MonoBehaviour {

	public Vector2 InitCoordinate;

	public RoadBoardBuilder roadBoardBuilder;
	Result[,] roadResult;

	public Result PreviousResult = Result.Blank;
    public Vector2 CurrentPoint = new Vector2(0, 0);

	int PlusPointX = 0;
    int LeadingX = 0;

	void Start(){
		roadBoardBuilder = GetComponent<RoadBoardBuilder>();
		roadResult = new Result[roadBoardBuilder.xSize, roadBoardBuilder.ySize];
	}

	public void CreatePill(Result result, Vector2 point, bool predictorMode){
		
		Tile tile = roadBoardBuilder.RoadTiles[(int)point.x, (int)point.y].GetComponent<Tile>();
		tile.SetResult(result, predictorMode);

		if(!predictorMode){
			if(point.y <= 0){
				LeadingX = (int)point.x;
				PlusPointX = LeadingX;
			}else{
				PlusPointX = (int)point.x;
			}

			PreviousResult = result;
			roadResult[(int)point.x, (int)point.y] = result;
			CurrentPoint = point;
		}

	}

	public Vector2 GetNewPillPoint(Result result){

		
		if(PreviousResult == Result.Blank){
			return new Vector2(0,0);
		}else if(PreviousResult != result){
			
			Vector2 point = new Vector2(0,0);
			for (int x = LeadingX; x < roadBoardBuilder.xSize; x++)
			{
				if(roadResult[x, 0] != Result.Blank){
					point = new Vector2(x + 1 , 0);
					break;
				}
			}
			return point;
		}else{
			if(LeadingX == PlusPointX && CurrentPoint.y < 5 && roadResult[PlusPointX, (int)CurrentPoint.y + 1] == Result.Blank){
				return new Vector2(PlusPointX, CurrentPoint.y + 1);
			}else{
				return new Vector2(PlusPointX + 1, CurrentPoint.y);
			}
		}
	}
	
	private Result GetPillNeedCreate(GameObject[,] mainRoadResult, Vector2 mainRoadPoint){
		if(!mainRoadResult[(int)InitCoordinate.x + 1,0].GetComponent<Tile>().CompareResult(Result.Blank) 
			|| !mainRoadResult[(int)InitCoordinate.x, (int)InitCoordinate.y].GetComponent<Tile>().CompareResult(Result.Blank) ){
				
				if(mainRoadPoint.y <= 0){
					int firstColumnAmount = 0;
					int secondColumnAmount = 0;

					for (int y = 0; y < 6; y++)
					{
						if(!mainRoadResult[(int)mainRoadPoint.x - 1 , y].GetComponent<Tile>().CompareResult(Result.Blank) ){
							firstColumnAmount++;
						}
						if(!mainRoadResult[(int)mainRoadPoint.x - 1 - (int)InitCoordinate.x , y].GetComponent<Tile>().CompareResult(Result.Blank)){
							secondColumnAmount++;
						}
					}

					if(firstColumnAmount == secondColumnAmount){
						return Result.Banker;
					}else{
						return Result.Player;
					}
				}else{
					if(!mainRoadResult[(int)mainRoadPoint.x - (int)InitCoordinate.x, (int)mainRoadPoint.y].GetComponent<Tile>().CompareResult(Result.Blank)){
						return Result.Banker;
					}else{
						if(mainRoadResult[(int)mainRoadPoint.x - (int)InitCoordinate.x, (int)mainRoadPoint.y - 1].GetComponent<Tile>().CompareResult(Result.Blank)){
							return Result.Banker;
						}else{
							return Result.Player;
						}
					}
				}
			}else{
				return Result.Blank;
			}
		
	}

	public virtual void BankerBtnPressed(GameObject[,] mainRoadTile, Vector2 mainRoadPoint){
		bool predictorMode = false;
		Result result = GetPillNeedCreate(mainRoadTile, mainRoadPoint);
		if(result != Result.Blank){
			Vector2 newPillPoint = GetNewPillPoint(result);
			CreatePill(result, newPillPoint, predictorMode);
		}
	}

	public virtual void BankerPredictorBtnPressed(GameObject[,] mainRoadTile, Vector2 mainRoadPoint){
		bool predictorMode = true;
		Result result = GetPillNeedCreate(mainRoadTile, mainRoadPoint);
		if(result != Result.Blank){
			Vector2 newPillPoint = GetNewPillPoint(result);
			CreatePill(result, newPillPoint, predictorMode);
		}
	}

	public virtual void PlayerBtnPressed(GameObject[,] mainRoadTile, Vector2 mainRoadPoint){
		bool predictorMode = false;
		Result result = GetPillNeedCreate(mainRoadTile, mainRoadPoint);
		if(result != Result.Blank){
			Vector2 newPillPoint = GetNewPillPoint(result);
			CreatePill(result, newPillPoint, predictorMode);
		}
	}

	public virtual void PlayerPredictorBtnPressed(GameObject[,] mainRoadTile, Vector2 mainRoadPoint){
		bool predictorMode = true;
		Result result = GetPillNeedCreate(mainRoadTile, mainRoadPoint);
		if(result != Result.Blank){
			Vector2 newPillPoint = GetNewPillPoint(result);
			CreatePill(result, newPillPoint, predictorMode);
		}
	}

	public Vector2 getCurrentPoint(){ return CurrentPoint; }
}
